import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';

import Arrows from '../components/Arrows';

import '../styles/pages/slide2.scss';

const Slide2 = ({ t }) => (
  <>
    <h1>{t('name')}</h1>
    <Arrows />
  </>
);

Slide2.propTypes = {
  t: PropTypes.func.isRequired
};

export default withTranslation('slide2')(Slide2);


