import React from 'react';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';

import Arrows from '../components/Arrows';

import '../styles/pages/slide1.scss';

const Slide1 = ({ t }) => (
  <>
    <h1>{t('name')}</h1>
    <Arrows />
  </>
);

Slide1.propTypes = {
  t: PropTypes.func.isRequired
};

export default withTranslation('slide1')(Slide1);


