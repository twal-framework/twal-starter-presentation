module.exports = {
  i18n: {
    namespaces: ['slide1', 'slide2', 'nav-links']
  },
  routing: {
    linksNamespace: 'nav-links',
    routes: [
      { component: 'Slide1' },
      { component: 'Slide2' }
    ]
  },
  navigation: {
    showLanguages: false
  },
  build: {
    folderName: 'cordova',
    id: 'io.twal.demo',
    fullScreen: true,
    orientation: 'landscape',
    platforms: [{ name: 'android', spec: '@7.1.4' }, { name: 'browser', spec: '@latest' }]
  }
};
